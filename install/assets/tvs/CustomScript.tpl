/**
 * CustomScript
 *
 * Free placeholder to add custom js into the template footer
 *
 * @category tv
 * @internal @modx_category Start
 * @internal @caption CustomScript
 * @internal @input_type text
 * @internal @input_options
 * @internal @input_default
 * @internal @output_widget
 * @internal @output_widget_params
 * @internal @lock_tv 0
 * @internal @template_assignments StartPage,StartHome-FlexSlider,StartHome-NivoSlider
 * @internal @installset base, sample
 */