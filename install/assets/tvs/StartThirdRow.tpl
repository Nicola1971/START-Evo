/**
 * StartThirdRow
 *
 * Add a third Bootstrap Row chunk
 *
 * @category tv
 * @internal @modx_category Start
 * @internal @caption StartThirdRow
 * @internal @input_type dropdown
 * @internal @input_options none== ||StartServices=={{StartServices}}||StartMap=={{StartMap}}
 * @internal @input_default
 * @internal @output_widget
 * @internal @output_widget_params
 * @internal @template_assignments *
 * @internal @lock_tv 0
 * @internal @installset base, sample
 */