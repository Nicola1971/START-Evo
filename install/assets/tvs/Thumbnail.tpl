/**
 * Thumbnail
 *
 * Thumbnail for pages
 *
 * @category tv
 * @internal @modx_category Start
 * @internal @caption Thumbnail image
 * @internal @input_type image
 * @internal @input_options
 * @internal @input_default
 * @internal @output_widget
 * @internal @output_widget_params
 * @internal @template_assignments StartPage
 * @internal @lock_tv 0
 * @internal @installset base, sample
 */