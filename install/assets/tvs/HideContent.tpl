/**
 * HideContent
 *
 * Hide Content ?
 *
 * @category tv
 * @internal @modx_category start
 * @internal @caption HideContent
 * @internal @input_type dropdown
 * @internal @input_options yes==yes||no==no
 * @internal @input_default no
 * @internal @output_widget
 * @internal @output_widget_params
 * @internal @lock_tv 0
 * @internal @template_assignments StartPage,StartHome-FlexSlider,StartHome-NivoSlider
 * @internal @installset base, sample
 */