/**
 * StartHead
 *
 * HEAD tag
 *
 * @category	chunk
 * @internal @modx_category Start
 */
<meta name="viewport" content="width=device-width, initial-scale=1.0">
{{Seo4Evo-metaTags}}
<link href="assets/templates/start/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/templates/start/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="assets/templates/start/css/main.css" rel="stylesheet">
<link href="assets/templates/start/js/wow/libs/animate.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="assets/templates/start/js/html5shiv.js"></script>
    <script src="assets/templates/start/js/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="[(site_url)]/favicon.gif" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/templates/start/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/templates/start/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/templates/start/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/templates/start/images/ico/apple-touch-icon-57-precomposed.png">