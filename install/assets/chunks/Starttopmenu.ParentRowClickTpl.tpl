/**
 * Starttopmenu.ParentRowClickTpl
 *
 * topmenu ParentRowTpl - open submenu on click
 *
 * @category	chunk
 * @internal @modx_category StartTopMenu
 */
<li[+wf.id+][+wf.classes+]><a class="dropdown-toggle" data-toggle="dropdown" href="[+wf.link+]" title="[+wf.title+]">[+wf.linktext+] <i class="fa fa-caret-down"></i></a>[+wf.wrapper+]</li>