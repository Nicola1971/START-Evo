/**
 * StartTopMenu
 *
 * Start top menu
 *
 * @category	chunk
 * @internal @modx_category StartTopMenu
 */
[[Wayfinder?startId=`0` &level=`2` &outerTpl=`Starttopmenu.OuterTpl.Left` &innerTpl=`Starttopmenu.InnerTpl` &rowTpl=`Starttopmenu.RowTpl` &innerRowTpl=`Starttopmenu.InnerRowTpl` &parentRowTpl=`Starttopmenu.ParentRowHoverTpl` &firstClass=`first` &hereClass=`active`]]