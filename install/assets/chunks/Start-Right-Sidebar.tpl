/**
 * Start-Right-Sidebar
 *
 * Start Right Sidebar Template
 *
 * @category	chunk
 * @internal @modx_category start
 */
<!--Sidebar -->
<aside class="right-sidebar white nopadding [+CRClass+]">
		<h2>Start Right Sidebar</h2>
<div class="widget">	
[[Ditto? &parents=`2` &display=`2` &total=`2` &removeChunk=`Comments` &tpl=`Start-news-sidebar`]]
[[Ditto? &parents=`2` &display=`2` &tpl=`Start-news-sidebar-small` &paginate=`0` &extenders=`summary,dateFilter`]]
</div>

</aside>
	<!-- End Sidebar-->