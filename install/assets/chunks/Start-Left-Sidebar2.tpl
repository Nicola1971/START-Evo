/**
 * Start-Left-Sidebar2
 *
 * Start Left Sidebar 2 Template
 *
 * @category	chunk
 * @internal @modx_category start
 */
<!--Sidebar -->
<aside class="left-sidebar white nopadding [+CLClass+]">
		<h2>Start Left Sidebar 2</h2>
<div class="widget">
<div id="search"><a name="search"></a>[!AjaxSearch? &ajaxSearch=`1` &landingPage=`8` &moreResultsPage=`8` &addJscript=`0` &showIntro=`0` &ajaxMax=`5` &extract=`1`!]</div>	
</div>		
<div class="widget">
<h3>Ready to START</h3>
<i class="fa fa-play-circle fa-4x fa-fw sideicon"></i><p>Start your MODx responsive site in few minutes</p>
</div>
<div class="widget">
<h3>Responsive</h3>
<i class="fa fa-mobile fa-4x fa-fw sideicon"></i><p>Responsive Bootstrap layout</p>
</div>
<div class="widget">
<h3>Bootstrap Columns</h3>
<i class="fa fa-columns fa-4x fa-fw sideicon"></i><p>Bootstrap Columns Plugin</p>
</div>
<div class="widget">
<h3>Sliders</h3>
<i class="fa fa-sliders fa-4x fa-fw sideicon"></i><p>NivoSlider and FlexSlider</p>
</div>
<div class="widget">
<h3>SwipeBox Gallery</h3>
<i class="fa fa-picture-o fa-4x fa-fw sideicon"></i><p>NovoSlider and FlexSlider</p>
</div>
<div class="widget">
<h3>Seo4Evo</h3>
<i class="fa fa-line-chart fa-4x fa-fw sideicon"></i><p>NovoSlider and FlexSlider</p>
</div>
	
</aside>
	<!-- End Sidebar-->