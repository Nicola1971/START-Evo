/**
 * Start-flexslider-css
 *
 * <strong>2.0</strong> include css for flexslider multitv
 *
 * @category	chunk
 * @internal @modx_category MultiTV add-ons
 */
<link rel="stylesheet" type="text/css" href="assets/templates/start/js/flexslider/flexslider.css" media="screen" />