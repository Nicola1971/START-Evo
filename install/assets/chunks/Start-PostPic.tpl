/**
 * Start-PostPic
 *
 * Picture in articles and pages
 *
 * @category	chunk
 * @internal @modx_category Start
 */
<div class="blog-img" itemprop="image">
                        <img src="[[phpthumb? &input=`[*Thumbnail*]` &options=`w=845,h=395,zc=TL`]]" alt="[*pagetitle*]" class="img-responsive img-thumbnail img-rounded"/>
                    </div>