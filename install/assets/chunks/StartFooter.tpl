/**
 * StartFooter
 *
 * Start template footer
 *
 * @category	chunk
 * @internal @modx_category Start
 */
<div class="col-sm-6">
	Copyright &copy; 2015 <a href="[(site_url)]">[(site_name)]</a> All Rights Reserved
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                    <li>Start (Beta 2.5) Template by <a href="http://www.tattoocms.it/">tattoocms.it</a></li>

                    </ul>
                </div>