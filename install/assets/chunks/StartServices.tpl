/**
 * StartServices
 *
 * Service row
 *
 * @category	chunk
 * @internal @modx_category Start
 */
<section id="services" class="blue">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div class="gap"></div>
        <div id="servicerow2" class="row">
                      <div class="col-md-3 col-xs-6">
                <div class="center wow fadeInLeftBig" data-wow-offset="300">
                    <p><a href="#"><i class="fa fa-columns icon-lg"></i></a></p>
                    <h3>Bootstrap Columns Plugin</h3>
                </div>
            </div>

                       <div class="col-md-3 col-xs-6">
                <div class="center wow fadeInLeftBig" data-wow-offset="200">
                    <p><a href="#"><i class="fa fa-sliders icon-lg"></i></a></p>
                    <h3>Nivo Slider Multitv</h3>

                </div>
            </div>

            <div class="col-md-3 col-xs-6">
                <div class="center wow fadeInRightBig" data-wow-offset="200">
                    <p><a href="#"><i class="fa fa-sliders icon-lg"></i></a></p>
                    <h3>Flex Slider Multitv</h3>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="center wow fadeInRightBig" data-wow-offset="300">
                    <p><a href="#"><i class="fa fa-picture-o icon-lg"></i></a></p>
                     <h3>SwipeBox Mini Gallery</h3>
                                 </div>
            </div>
             <div class="col-md-2 col-xs-6">
                       </div>

                </div>
            </div>
        </div>
			<!--row2-->
			<div class="row">


                <div class="col-lg-12">
                    <div class="gap"></div>


        <div id="servicerow2" class="row">
                      <div class="col-md-3 col-xs-6">
                <div class="center wow bounceInLeft" data-wow-offset="400">
                    <p><a href="#"><i class="fa fa-line-chart icon-lg"></i></a></p>
                    <h3>Seo4Evo</h3>

                </div>
            </div>

                       <div class="col-md-3 col-xs-6">
                <div class="center wow bounceInLeft" data-wow-offset="400">
                    <p><a href="#"><i class="fa fa-tablet icon-lg"></i></a></p>
                    <h3>Responsive</h3>

                </div>
            </div>

            <div class="col-md-3 col-xs-6">
                <div class="center wow bounceInRight" data-wow-offset="400">
                    <p><a href="#"><i class="fa fa-flag icon-lg"></i></a></p>
                    <h3>Font Awesome</h3>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="center wow bounceInRight" data-wow-offset="400">
                    <p><a href="#"><i class="fa fa-html5 icon-lg"></i></a></p>
                    <h3>HTML5 & CSS3</h3>
                                 </div>
            </div>
             <div class="col-md-2 col-xs-6">
                       </div>

                </div>
            </div>
        </div>

			<!--end-->
		</div>
    </section>