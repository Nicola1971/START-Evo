/**
 * Start-tplPaginatePage
 *
 * Bootstrap Pagination
 *
 * @category	chunk
 * @internal @modx_category Start Bootstrap Pagination
 */
<li><a href="[+url+]">[+page+]</a></li>