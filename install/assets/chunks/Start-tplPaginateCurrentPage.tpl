/**
 * Start-tplPaginateCurrentPage
 *
 * Bootstrap Pagination
 *
 * @category	chunk
 * @internal @modx_category Start Bootstrap Pagination
 */
<li class="active"><span>[+page+]</span></li>