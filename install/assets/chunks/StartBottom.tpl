/**
 * StartBottom
 *
 * Start Bottom row
 *
 * @category	chunk
 * @internal @modx_category Start
 */
<section id="bottom" class="grey">
        <div class="container">
            <div class="row">
<div class="col-md-3 col-sm-6">
                    <h4>Footer Links</h4>
                     <nav class="footer_nav">
         <ul class="list-unstyled">
<li><a href="#">Item link</a></li>
<li><a href="#">Item link</a></li>
<li><a href="#">Item link</a></li>
<li><a href="#">Item link</a></li>
</ul>
      </nav>
</div>

<div class="col-md-3 col-sm-6">
                    <h4>Footer Links</h4>
                     <nav class="footer_nav">
         <ul class="list-unstyled">
<li><a href="#">Item link</a></li>
<li><a href="#">Item link</a></li>
<li><a href="#">Item link</a></li>
<li><a href="#">Item link</a></li>
</ul>
      </nav>
</div>
			

<div class="col-md-3 col-sm-6">
                    <h4>Contacts</h4>
                    <address>
                     Lorem ipsum dolor sit amet<br>
                        <abbr title="Phone">tel.</abbr> 0123456789
                    </address>
</div>

<div class="col-md-3 col-sm-6">
					<div class="media">
                    <h4>Social Network</h4>
                    <div>
                     <ul class="list-unstyled">
					 <li><a class="btn btn-social btn-facebook" href="#"><i class="fa fa-facebook fa-2x"></i></a>  Facebook</li>
                  <li><a class="btn btn-social btn-twitter" href="#"><i class="fa fa-twitter fa-2x"></i></a> Twitter</li>
                  <li><a class="btn btn-social btn-google-plus" href="#"><i class="fa fa-google-plus fa-2x"></i></a> Google+</li>
                    <li><a class="btn btn-social btn-linkedin" href="#"><i class="fa fa-linkedin fa-2x"></i></a> Linkedin</li>
                   </ul></div>
                    </div>
                </div>
            </div>
        </div>
    </section>