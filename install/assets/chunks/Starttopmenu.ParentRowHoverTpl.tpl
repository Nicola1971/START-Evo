/**
 * Starttopmenu.ParentRowHoverTpl
 *
 * topmenu ParentRowTpl - open submenu on Hover
 *
 * @category	chunk
 * @internal @modx_category StartTopMenu
 */
<li class="dropdown"><a href="[+wf.link+]" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="false" title="[+wf.title+]">[+wf.linktext+] <i class="fa fa-caret-down"></i></a>[+wf.wrapper+]</li>