/**
 * Starttopmenu.OuterTpl.Right
 *
 * Starttopmenu.OuterTpl Right nav
 *
 * @category	chunk
 * @internal @modx_category StartTopMenu
 */
<ul class="nav navbar-nav navbar-right">
    [+wf.wrapper+]
     </ul>